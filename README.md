# file-synchronizer-web
This is simple webapp implementing SOAP Service with Yii2 for synchronizing local files to web server.


Currently there is no data display provider - it'll be added in the future.


WSDL is located under route: 
> index.php?r=soap/synchronizer

## how-to
I've included sql (workbench) model file in the build/sql/model directory.

After forwarding it to database, remember to execute init.sql file on that database.

Now you can copy and edit configuration from build/skel/config directory.

