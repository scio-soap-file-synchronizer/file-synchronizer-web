<?php

ini_set('error_reporting', E_ALL);

//defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require( __DIR__ . '/../Scio/core/autoloader/ScioAutoloader.php' );
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new Scio\core\app\web\ScioApplication($config))->run();
