<?php

namespace Scio\synchronizer\session;

use app\models\ar\SoapSession;
use Scio\core\db\SCIODBManager;
use yii\base\Object;
use yii\db\Exception;


class ScioSessionService extends Object implements SOAPSessionProvider{
	private $login;
	private $password;
	private $scioDBManager;
	
	public function __construct( $wsLogin, $wsPassword, $algo, SCIODBManager $scioDBManager, $config = array() ){
		parent::__construct( $config );
		
		$this->login = $wsLogin;
		$this->password = hash( $algo, $wsPassword );
		$this->scioDBManager = $scioDBManager;
	}
	
	public function authenticate( $login, $password ){
		if( $login === $this->login && $password === $this->password ){
			return $this->handleSessionCreate();
		}else{
			throw new Exception( "AuthException: invalid login or password" );
		}
	}

	public function getIsSessionValid( $sessionID ){
		return $this->checkIsSessionValid( $sessionID );
	}
	
	private function checkIsSessionValid( $sessionID ){
		$sql = "SELECT 1 FROM soap_session WHERE aid = :session_id";
		$parameters = [ 'session_id' => $sessionID ];
		
		return $this->scioDBManager->sqlQueryExists( $sql, $parameters );
	}

	// @todo multiple sessions, cleaning table once per 24h
	private function handleSessionCreate(){
		return $this->scioDBManager->getConnection()->transaction( function(){
			SoapSession::deleteAll();
		
			$soapSession = new SoapSession();
			$soapSession->aid = uniqid( "sess_", TRUE );
			$soapSession->creation_date_and_time = date('Y-m-d H:i:s');
			$soapSession->save();

			return $soapSession->aid;
		});
	}

}
