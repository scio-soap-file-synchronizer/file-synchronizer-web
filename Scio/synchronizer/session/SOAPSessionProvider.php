<?php

namespace Scio\synchronizer\session;

interface SOAPSessionProvider{
	/**
	 * @param string $login
	 * @param string $password hashed password
	 * @throws \Exception 
	 * @return string returns sessionID
	 */
	public function authenticate( $login, $password );
	
	/**
	 * @param string $session
	 * @return bool is session id valid
	 */
	public function getIsSessionValid( $session );
}
